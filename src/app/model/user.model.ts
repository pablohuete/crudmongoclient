export class User {

    _id: string;
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
    is_active: boolean;
    is_verified: boolean;
    is_deleted: boolean;
    createdAt: string;
    updatedAt: string;

}
