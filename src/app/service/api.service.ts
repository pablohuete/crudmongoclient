import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/index';
import { User } from '../model/user.model'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:4000/api/users'


  login(loginPayload) : Observable<any> {
    return this.http.post<any>('http://localhost:4000/api/' + 'login', loginPayload, { observe : 'response'});
  }

  getUsers(): Observable<any> {
    return this.http.get<any>(this.baseUrl, { observe: 'response' });
  }

  getUserById(id: string): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/' + id, { observe: 'response' });
  }

  createUser(user: User): Observable<any> {
    return this.http.post<any>(this.baseUrl, user, { observe: 'response' });
  }

  updateUser(user: User): Observable<any> {
    return this.http.put<any>(this.baseUrl + '/' + user._id, user, { observe: 'response' });
  }

  deleteUser(id: string): Observable<any> {
    return this.http.delete<any>(this.baseUrl + '/' + id, { observe: 'response' });
  }
}
