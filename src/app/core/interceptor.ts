import { HttpEvent, HttpHandler, HttpRequest, HttpInterceptor } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";
import { Injectable } from "@angular/core";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
        let token = window.localStorage.getItem('token');
        if (!token) {
           token = btoa(request.body.username + ':' + request.body.password);
        }
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Basic ' + token
                }
            });
        }
        return next.handle(request);
    }
}