import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { User } from '../../model/user.model';
import { ApiService } from '../../service/api.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  users: User[];

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    if (!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.apiService.getUsers()
      .subscribe(response => {
        this.users = response.body;
      });
  }

  deleteUser(user: User): void {
    let c = confirm("Are you sure?");
    if(c){
      this.apiService.deleteUser(user._id)
        .subscribe(data => {
          this.users = this.users.filter(u => u !== user);
        })
    }
  };

  editUser(user: User): void {
    window.localStorage.removeItem("editUserId");
    window.localStorage.setItem("editUserId", user._id.toString());
    this.router.navigate(['edit-user']);
  };

  addUser(): void {
    this.router.navigate(['add-user']);
  };
}
