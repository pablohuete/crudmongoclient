import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ApiService } from "../../service/api.service";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) { }

  addForm: FormGroup;
  ngOnInit() {
    if (!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.addForm = this.formBuilder.group({
      _id: [],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      is_verified: [false, Validators.required],
      is_active: [false, Validators.required]
    });
  }

  onSubmit() {
    this.apiService.createUser(this.addForm.value)
      .subscribe(response => {
        alert('User created successfully.')
        this.router.navigate(['list-user']);
      });
  }
  goBack() {
    this.router.navigate(['list-user']);
  }
}
