import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { first } from "rxjs/operators";
import { User } from "../../model/user.model";
import { ApiService } from "../../service/api.service";
import { error } from 'util';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  user: User;
  editForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    if (!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }

    let userId = window.localStorage.getItem("editUserId");
    if (!userId) {
      alert("Invalid action.")
      this.router.navigate(['list-user']);
      return;
    }
    this.editForm = this.formBuilder.group({
      _id: [''],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      is_verified: ['', Validators.required],
      is_active: ['', Validators.required]
    });
    this.apiService.getUserById(userId)
      .subscribe(response => {
        this.editForm.setValue({
          _id: response.body['_id'],
          first_name: response.body['first_name'],
          last_name: response.body['last_name'],
          email: response.body['email'],
          phone: response.body['phone'],
          is_verified: response.body['is_verified'],
          is_active: response.body['is_active']
        });
      });
  }

  onSubmit() {
    this.apiService.updateUser(this.editForm.value)
      .pipe(first())
      .subscribe(
        response => {
          debugger;
          if (response.status === 200) {
            alert('User update successfully.')
            this.router.navigate(['list-user']);
          }
          else {
            alert(response.statusText);
          }
        },
        error => {
          alert(error);
        }
      );
  }

  goBack() {
    this.router.navigate(['list-user']);
  }

}
